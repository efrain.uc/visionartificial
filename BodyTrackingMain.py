import cv2
import time
import BodyTrackingModule as BTM

cap = cv2.VideoCapture('resources/4.mp4')
pTime = 0
detector = BTM.poseDetector()
while 1:
    sucess, img = cap.read()
    img = detector.findPose(img)
    lmList = detector.findPosition(img, draw=False)
    if lmList:
        cv2.circle(img, (lmList[14][1], lmList[14][2]), 5, (0,255,0), cv2.FILLED)
    cTime = time.time()
    fps = 1/(cTime-pTime)
    pTime = cTime
    cv2.putText(img, str(int(fps)), (70,50), cv2.FONT_HERSHEY_PLAIN, 3, (255,0,0), 3)
    cv2.imshow("test", img)
    cv2.waitKey(1)