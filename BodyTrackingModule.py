import cv2
import mediapipe as mp

class poseDetector():
    def __init__(self, mode=False, upBody=False, smooth = True, minDetection=0.5, minTracking=0.5):
        self.mode = mode
        self.upBody = upBody
        self.smooth = smooth
        self.minDetection = minDetection
        self.minTracking = minTracking

        self.mpDraw = mp.solutions.drawing_utils
        self.mpPose = mp.solutions.pose
        self.pose = self.mpPose.Pose(self.mode)

    def findPose(self, img, draw = True):
        imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.results = self.pose.process(imgRGB)

        if self.results.pose_landmarks:
            if draw:
                self.mpDraw.draw_landmarks(img, self.results.pose_landmarks, self.mpPose.POSE_CONNECTIONS)
        return img
    
    def findPosition(self, img, draw = True):
        lmList = []
        if self.results.pose_landmarks:
            for id, lm in enumerate(self.results.pose_landmarks.landmark):
                h, w, c = img.shape
                cx, cy = int(lm.x * w), int(lm.y * h)
                lmList.append([id, cx, cy])
                if draw:
                    cv2.circle(img, (cx,cy), 10, (0,255,0), cv2.FILLED)
            
        return lmList

def main():
    print("BodyTrackingModule")

if __name__ == "__main__":
    main()