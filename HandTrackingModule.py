import cv2
import time
import mediapipe as mp

class handDetector():
    def __init__(self, mode=False, maxHands=2, minDetection=0.5, minTracking=0.5):
        self.mode = mode
        self.maxHands = maxHands
        self.minDetection = minDetection
        self.minTracking = minTracking

        self.mpHands = mp.solutions.hands
        self.hands = self.mpHands.Hands(self.mode, self.maxHands)
        self.mpDraw = mp.solutions.drawing_utils

    def findHands(self, img, draw = True):
        imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.results = self.hands.process(imgRGB)
        if self.results.multi_hand_landmarks:
            for handLms in self.results.multi_hand_landmarks:
                if(draw):
                    self.mpDraw.draw_landmarks(img, handLms, self.mpHands.HAND_CONNECTIONS)
        return img
    
    def findPosition(self, img, handNumber = 0, draw = True, radio = 5, color = (0, 0, 255)):
        lmList = []
        if self.results.multi_hand_landmarks:
            myHand = self.results.multi_hand_landmarks[handNumber]
            for id, lm in enumerate(myHand.landmark):
                #print(id, lm)
                heigh, width, c = img.shape
                cx, cy = int(lm.x*width), int(lm.y*heigh)
                lmList.append([id, cx, cy])
                if draw:
                    cv2.circle(img, (cx, cy), radio, color, cv2.FILLED)

        return lmList
    
def main():
    print("HandTrackingModule")

if __name__ == "__main__":
    main()